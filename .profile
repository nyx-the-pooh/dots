# PATH
PATH="/bin:/sbin:/usr/bin:/usr/sbin:/usr/local/bin:/usr/local/sbin"
PATH="$HOME/usr/bin:$HOME/usr/bin/scripts:$PATH"
export PATH

# MANPATH
MANPATH="$MANPATH:$HOME/usr/share/man/scheme"
MANPATH="$MANPATH:/usr/local/man/"
export MANPATH

# Default programs
export EDITOR="kak"
export VISUAL="$EDITOR"
export PAGER="less -ri"
export SUDO="doas"
export TERMINAL="xst"
export FZF="pick"
export BROWSER="firefox"

# ~/ Clean-up:
export XDG_CONFIG_HOME="$HOME/etc"
export XDG_DATA_HOME="$HOME/usr/share"
export XDG_CACHE_HOME="$HOME/var/cache"
export XDG_STATE_HOME="$HOME/var/lib"

# etc
export ENV="$XDG_CONFIG_HOME/shell/shrc"
export INPUTRC="$XDG_CONFIG_HOME/shell/inputrc"
export GTK2_RC_FILES="$XDG_CONFIG_HOME/gtk-2.0/gtkrc-2.0"
export WGETRC="$XDG_CONFIG_HOME/wget/wgetrc"
export ZDOTDIR="$XDG_CONFIG_HOME/zsh"
export INPUTRC="$XDG_CONFIG_HOME/shell/inputrc"
export NPM_CONFIG_USERCONFIG="$XDG_CONFIG_HOME/npm/npmrc"
export XINITRC="$XDG_CONFIG_HOME/x11/xinitrc"
export ELINKS_CONFDIR="$XDG_CONFIG_HOME/elinks"
export PYTHONSTARTUP="$XDG_CONFIG_HOME/python/pythonrc"
export _JAVA_OPTIONS="-Djava.util.prefs.userRoot=$XDG_CONFIG_HOME/java"

# usr/share
export GNUPGHOME="$XDG_DATA_HOME/gnupg"
export RUSTUP_HOME="$XDG_DATA_HOME/rustup"
export CARGO_HOME="$XDG_DATA_HOME/cargo"
export GOPATH="$XDG_DATA_HOME/go"
export HISTFILE="$XDG_DATA_HOME/history"
export PASSWORD_STORE_DIR="$XDG_DATA_HOME/password-store"
export MBLAZE="$XDG_DATA_HOME/mblaze"
export GERBIL_PATH="$XDG_DATA_HOME/gerbil"

# other
export LESSHISTFILE="-"
export TMUX_TMPDIR="$XDG_RUNTIME_DIR"
export XAUTHORITY="$XDG_RUNTIME_DIR/Xauthority"
export XBPS_DISTDIR="$HOME/usr/src/void-packages"
export QT_QPA_PLATFORMTHEME=qt5ct
export ABDUCO_SOCKET_DIR="$XDG_STATE_HOME/abduco"
export MAIL="$HOME/var/mail/INBOX"

# PATH
PATH="$CARGO_HOME/bin:$PATH"
PATH="$XDG_DATA_HOME/npm/bin:$PATH"
PATH="$GOPATH/bin:$PATH"
PATH="$GERBIL_PATH:$PATH"
export PATH

# tty colorscheme
"$XDG_CONFIG_HOME/shell/ttytheme"

# ssh agent
eval "$(keychain --eval --dir "$XDG_DATA_HOME/keychain" --agents "ssh" id_ed25519)"
