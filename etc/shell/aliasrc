#!/bin/sh

# Manage dotfiles
alias dots='git --git-dir=$HOME/usr/dotfiles --work-tree=$HOME'

# Move wget-hsts file to XDG_DATA_HOME
# alias wget='wget --hsts-file=$XDG_DATA_HOME/wget-hsts'

# Package management
alias yeet='doas xbps-remove -Ro'
alias xsearch='xbps-query -Rs'

# ls shortcuts
alias ls='ls -p'
alias la='ls -a'
alias ll='ls -lah'

# Misc
alias bigfiles='du -h --max-depth=1 2> /dev/null | sort -n -r | head -n20'
command -v doas >/dev/null || alias doas=sudo

# Common programs
alias vi='$EDITOR'
alias yta='yt-dlp -x -f bestaudio/best --audio-format opus'
alias ytm="yt-dlp -x -f bestaudio/best --audio-format opus -o '~/usr/audio/music/%(artist)s/%(album)s/%(track)s.%(ext)s'"

# Mblaze
alias new='mlist -s $MAIL'
alias all='mlist -s $MAIL | mthread | mless'
alias mall='mlist -s $MAIL | mthread | mseq -S ; mseq -C 1'
alias mread='mseq | mflag -S'
alias s='mshow | less'
alias n='mshow .+1'
alias p='mshow .-1'
alias sc='mscan'
alias mrepc='mrep $(mseq .)'

# Git
alias gs='git status --short --branch || ls'
alias gsh='git show'
alias gl='git log --graph --pretty=log'
alias gco='git checkout' gb='git branch' gm='git merge' gst='git stash'
alias ga='git add' gmv='git move' grm='git rm'
alias gc='git commit' gca='gc --amend' gt='git tag'
alias gp='git push' gu='git pull' gf='git fetch'
alias gr='git rebase' grc='gr --continue'
alias cdg='cd "$(git rev-parse --show-toplevel)" || return'
alias gls='git ls-tree -r HEAD --name-only'
alias gg='git grep "TODO:\|FIXME:\|BUG:\|NYI"'

# Shell functions
ffyt() {
	yt-dlp "$1" -o - | ffplay - -autoexit -loglevel quiet
}

ffytm() {
	yt-dlp "$1" -o - | ffplay - -autoexit -loglevel quiet -vn -nodisp
}

md() {
	lowdown -tterm "$1" | kak
}

mdc() {
	sed -n '/^```/,/^```/ p' < $1 | sed '/^```/ d'
}

urlencode() {
	old_lc_collate=$LC_COLLATE
	LC_COLLATE=C
	i=1
	length="${#1}"
	while [ "$i" -le "$length" ]; do
		c="$(echo "$1" | cut -c "$i")"
		case $c in
		[a-zA-Z0-9.~_-]) printf "%s" "$c" ;;
		' ') printf "%%20" ;;
		*) printf '%%%02X' "'$c" ;;
		esac
		i=$((i + 1))
	done

	LC_COLLATE=$old_lc_collate
}

urldecode() {
	printf '%b\n' "$(sed -E -e 's/\+/ /g' -e 's/%([0-9a-fA-F]{2})/\\x\1/g')"
}

o() {
	ft=$(file --mime-type "$1" -b)
	case $ft in
		inode/symlink) $0 "$(readlink "$1")" && exit ;;
		inode/directory) cd "$1" || exit ;;
		application/pdf) devour zathura "$1" ;;
		image/*) devour imv "$1" ;;
		text/*) ${EDITOR:-vi} "$1" ;;
		*) echo "unsupported filetype:" "$ft" ;;
	esac
}

gpgbak() {
	umask 077; tar -cf "$HOME/usr/doc/gnupg-backup.tar" "$GNUPGHOME"
}

